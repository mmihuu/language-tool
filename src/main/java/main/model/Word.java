package main.model;

public abstract class Word {

    public abstract void pronunciation();

    public abstract String translation(String str);


}
