package main.controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import main.model.WikiModel;
import main.sqlite.SQLiteDic;
import main.tools.CopyFromTheCliboard;
import org.apache.commons.lang3.StringUtils;

import java.net.URL;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PopController implements Initializable {


    static final int indexCounter = 0;


    @FXML
    public VBox vBox;
    @FXML
    public ScrollPane scrollPane;
    String toDataBase;
    @FXML
    private Text InputText;

    public void initDate() throws SQLException {



        scrollPane.setFitToHeight(true);


        WikiModel w = new WikiModel();
        String stringToWork = w.getInfo(CopyFromTheCliboard.copyTheStringClipBoard());
        stringToWork = " "+stringToWork;
        System.out.println(stringToWork);
        String s = StringUtils.substringBetween(stringToWork, String.valueOf(stringToWork.charAt(0)), "({{");
        System.out.println("!!!!!!!!!!!!!!!!!!!"+s+"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        stringToWork = s.replace("[", "")
                .replace("]", "").replace(",", "-").replace("\'", "")
                .replace("{{lp}}", "<l.p>").replace("{{lm}}", "<l.m>");
        stringToWork = "{{" + stringToWork;
        System.out.println(stringToWork);


        String[] split = stringToWork.split("\\{\\{");

        split = Arrays.stream(split)
                .filter(str -> (str != null && str.length() > 0 && str.indexOf(":") != -1))
                .toArray(String[]::new);

        int index = 0;
        int length = split.length;
        Text[] t = new Text[length];
        Text[] titleArr = new Text[length];

        System.out.println(split.length + "ile slotów ma split");
        int lengthForException = split.length;
        if (split.length == 0) {
            t = new Text[length + 1];
            titleArr = new Text[length + 1];
            split = new String[1];
            split[0] = " nie ma tego recordu w wiktionary}}";
        }

        for (String a : split) {
            vBox.setPadding(new Insets(10, 10, 10, 10));

            System.out.println(a);

            String title = StringUtils.substringBetween(a, "", "}}");




            String formattedText = a.substring(title.length(), a.length()).replace(":", "*").replaceAll("[0-9]", "")
                    .replace("(.)", "").replace("}", "")
                    .replace("-", ",").replace("()", "");



            Pattern p = Pattern.compile("[a-zA-Z]");


            Matcher m = p.matcher(formattedText);
            if(m.find()) {
                titleArr[index] = new Text(title);


                titleArr[index].setStyle("-fx-font-weight: bold; -fx-font-size: 18");
                t[index] = new Text(formattedText);

                t[index].wrappingWidthProperty().bind(vBox.widthProperty().subtract(40));
                titleArr[index].wrappingWidthProperty().bind(vBox.widthProperty().subtract(40));


                boolean add = vBox.getChildren().addAll(titleArr[index], t[index]);
                index++;

            }
            // font changing function, needs some tuning
            /*vBox.widthProperty().addListener((obs, oldVal, newVal) -> {
                System.out.println(newVal);
                int fontSize = newVal.intValue() / 24;
                int fontSizeTitle = newVal.intValue()/initialSizeOfAFont;
                t[indexForLambda].setFont(Font.font(fontSize));
                titleArr[indexForLambda].setFont(Font.font("Times New Roman",FontWeight.BOLD,fontSizeTitle));


            });*/


        }
        int separator = 0;
        if (lengthForException > 0) {
            SQLiteDic sqLiteDic = new SQLiteDic();

            separator = t[0].getText().indexOf("*");
            sqLiteDic.addWord(CopyFromTheCliboard.copyTheStringClipBoard(), t[0].getText().substring(separator + 1).trim()
                    .replaceAll("[^\\pL\\pN\\s\\,]", ""));

        }






    }


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {


    }
}
