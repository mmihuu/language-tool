package main.controller;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfDocument;
import com.itextpdf.text.pdf.PdfWriter;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import main.flashcards.CreateSetOfFlashcards;
import main.tools.PdfCreator;

import java.io.*;
import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ResourceBundle;

public class Controller implements Initializable {


    @FXML
    public Button inputButton;
    @FXML
    public Button flashCards;
    @FXML
    public Button testButton;
    @FXML
    AnchorPane anchorPane;
    @FXML
    TextArea translation;


    private boolean a = false, w = false;


    public Controller() {
        inputButton = new Button();
        flashCards = new Button();
        testButton = new Button();
        translation = new TextArea();


    }

    @FXML
    void initialize() {
        assert inputButton != null : "fx:id=\"button\" was not injected: check your FXML file 'mainpane.fxml'.";
        assert anchorPane != null : "fx:id=\"anchorePane\" was not injected: check your FXML file 'mainpane.fxml'.";
        inputButton.setMnemonicParsing(true);
        inputButton.setText("_Take a Word");


    }


    @FXML
    public void setOnAction(ActionEvent actionEvent) throws IOException, SQLException {

        FXMLLoader loaderFXM = new FXMLLoader();
        loaderFXM.setLocation(getClass().getResource("/fxml/copywindow.fxml"));

        Parent parentView = loaderFXM.load();
        AnchorPane bp = null;

        PopController p = loaderFXM.getController();
        p.initDate();


        Stage stage = new Stage();


        Scene scene = new Scene(parentView);

        stage.setTitle("Translation");

        stage.setScene(scene);
        stage.show();


    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }


    public void flashCardsOnAction(ActionEvent actionEvent) throws SQLException, ClassNotFoundException, IOException, DocumentException {

        Stage stage = new Stage();


        FileChooser fileChooser = new FileChooser();

        fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));


        FileChooser.ExtensionFilter extensionFilter = new FileChooser.ExtensionFilter("Pdf files", "*.pdf");
        fileChooser.getExtensionFilters().

                add(extensionFilter);

        File file = fileChooser.showSaveDialog(stage);


        Document doc = null;


        CreateSetOfFlashcards createSetOfFlashcards = new CreateSetOfFlashcards();


        doc = new Document(PageSize.A4);
        PdfWriter writer3 = PdfWriter.getInstance(doc, new FileOutputStream(
                file));


        doc.open();

        PdfCreator.addTitlePage(doc, "Flashcards", LocalDate.now(), LocalDate.now().minusDays(15));
        PdfCreator.addContent(doc, createSetOfFlashcards.resultsDates());



        doc.close();


    }

    public void testOnAction(ActionEvent actionEvent) throws SQLException, ClassNotFoundException, IOException {



        Test test = new Test();
        test.generateTest();






    }
}




