package main.controller;


import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.InputMethodEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import main.sqlite.SQLiteDic;
import main.test.ASetOfQuestions;

import java.awt.event.ActionListener;
import java.awt.event.TextEvent;
import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Test implements Initializable {

    @FXML
    public VBox pane;

    Label label;

    @FXML
    TextField[] text;

    Button submitResponse;


    public Test() {
        pane = new VBox();
        pane.setStyle("-fx-background-color: #0c2b36;");
        pane.setPadding(new Insets(30, 10, 10, 10));
        pane.setSpacing(5);
        pane.setAlignment(Pos.CENTER);


        pane.prefHeight(40);
        pane.prefWidth(30);
        pane.maxHeight(Double.MAX_VALUE);
        pane.maxHeight(Double.MAX_VALUE);

        label = new Label();
        label.setText("Test");
        label.setFont(Font.font(24));
        label.setTextFill(Color.valueOf("#d4243b"));


        pane.getChildren().add(label);


    }


    public void generateTest() {

        submitResponse = new Button("Submit answers");

        ASetOfQuestions as = new ASetOfQuestions();
        Map<String, String> mapToTest = as.getMapToTest();
        text = new TextField[mapToTest.size()];

        int counter = 0;
        for (Map.Entry<String, String> entry : mapToTest.entrySet()) {







            Text t = new Text(entry.getKey());

            t.setTextAlignment(TextAlignment.CENTER);
            pane.setVgrow(t, Priority.ALWAYS);

            t.setFont(Font.font(16));


            pane.getChildren().addAll(t);

            final int coun = counter;

            text[counter] = new TextField();
            text[counter].setPrefHeight(40);
            text[counter].setPrefWidth(40);

            pane.setVgrow(text[counter], Priority.ALWAYS);
            pane.getChildren().addAll(text[counter]);


            /*pane.setVgrow(text[counter], Priority.ALWAYS);
            pane.getChildren().addAll(text[counter]);*/

            text[counter].textProperty().addListener((observable, oldValue, newValue) -> {


                System.out.println(newValue);


                String toBeChecked = mapToTest.get(entry.getKey());
                toBeChecked = toBeChecked.replace("\n", "").replace(",", " ");

                String aslk = ".*\\b" + newValue + "\\b.*".trim();
                System.out.println(aslk);

                Pattern pattern = Pattern.compile(aslk);


                System.out.println(toBeChecked + " do sprawdzenia");
                Matcher matcher = pattern.matcher(toBeChecked);
                boolean matches = matcher.matches();
                System.out.println(text.length);
                if (matches) {
                    text[coun].setStyle("-fx-background-color: #054d29;\n" +
                            "-fx-background-image:url('/css/img/c.png');\n" +
                            "-fx-background-repeat: no-repeat;\n" +
                            "-fx-background-position: right;\n"
                            );

                }
            });


            counter++;
        }


        submitResponse.setMaxHeight(Double.MAX_VALUE);
        System.out.println(pane.getWidth());


        submitResponse.setWrapText(true);
        submitResponse.setAlignment(Pos.CENTER);


        pane.getChildren().addAll(submitResponse);


        Scene scene = new Scene(pane);
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.show();

    }

   /* public void textValueProvided(TextEvent te) {
        String text = this.text.getText();
        System.out.println(text);
    }*/


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {


    }


    public void textOnAction(ActionEvent actionEvent) {
        System.out.println(text[0].getText() + " tuta actuib ");
        String answer = (String) actionEvent.getSource();
        System.out.println(answer + " set on action");


    }

    public void OnInputMethodTextChanged(InputMethodEvent inputMethodEvent) {
        System.out.println(inputMethodEvent.toString());
        System.out.println(text[0].getText());

    }
}
