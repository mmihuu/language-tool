package main.test;

import main.sqlite.SQLiteDic;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.*;

public class ASetOfQuestions {


    public Map<String, String> getMapToTest() {
        return mapToTest;
    }

    Map<String, String> mapToTest = new LinkedHashMap<>();

    List<String> formattedAnswers = new ArrayList();

    public ASetOfQuestions() {
        try {
            createASet();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public List<String> getFormattedAnswers() {
        return formattedAnswers;
    }

    public void createASet() throws SQLException, ClassNotFoundException {


        SQLiteDic sqLiteDic = new SQLiteDic();
        ResultSet resultSet = sqLiteDic.displayWords();
        while (resultSet.next()) {


            mapToTest.put(resultSet.getString(1), getFormattedAnswers(resultSet.getString(2)));

        }
        System.out.print(mapToTest.entrySet());

    }


    public String getFormattedAnswers(String str) {


        str = str.replace(" ", "");
        str = str.trim();


        return str;
    }


}
