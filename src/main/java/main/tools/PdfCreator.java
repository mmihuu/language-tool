package main.tools;


import java.io.IOException;
import java.time.LocalDate;
import java.util.Map;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;



public class PdfCreator {

    final static int  count = 1;
    private final static String[] HEADER = {"Word", "Translation"};


    public final static Font SMALL_BOLD = new Font(Font.FontFamily.TIMES_ROMAN, 8,
            Font.BOLD);
    public final static Font NORMAL_FONT = new Font(Font.FontFamily.TIMES_ROMAN, 8,
            Font.NORMAL);
    public final static Font LARGE_FONT = new Font(Font.FontFamily.TIMES_ROMAN, 16,
            Font.NORMAL);
    public static void addMetaData(Document document, String sqlXMLFileName) {


    }
    public static void addContent(Document document, Map map) throws DocumentException {
        Paragraph paragraph = new Paragraph();
        paragraph.setFont(NORMAL_FONT);
        createReportTable(paragraph, map);
        document.add(paragraph);
    }
    private static void createReportTable(Paragraph paragraph, Map map)
            throws BadElementException {
        PdfPTable table = new PdfPTable(2);

        table.setWidthPercentage(100);
        paragraph.add(new Chunk("Translation :- ", SMALL_BOLD));
        if(map.isEmpty()){
            paragraph.add(new Chunk("No words saved in the DB."));
            return;
        }
        addHeaderInTable(HEADER, table);

        map.forEach((k, v)->{
            System.out.println(k.toString());
            System.out.println(v.toString());
            addToTable(table, k.toString());
            addToTable(table, v.toString());



        });


        paragraph.add(table);
    }
    /** Helper methods start here **/
    public static void addTitlePage(Document document, String title, LocalDate start, LocalDate end) throws DocumentException {
        Paragraph preface = new Paragraph();
        preface.add(new Phrase(title, PdfCreator.LARGE_FONT));
        addEmptyLine(preface, 1);
        preface.add(new Phrase("Words have been added to db from " + start.toString() + " to " + end.toString(), SMALL_BOLD));
        /*preface.add(new Phrase(new Date().toString(), NORMAL_FONT));*/

        document.addSubject("PDF : " + title);
        preface.setAlignment(Element.ALIGN_CENTER);
        document.add(preface);

    }
    public static void addEmptyLine(Paragraph paragraph, int number) {
        for (int i = 0; i < number; i++) {
            paragraph.add(new Paragraph(" "));
        }
    }
    public static void addHeaderInTable(String[] headerArray, PdfPTable table){
        PdfPCell c1 = null;
        for(String header : headerArray) {
            c1 = new PdfPCell(new Phrase(header, new Font(NORMAL_FONT)));
            c1.setBackgroundColor(BaseColor.GREEN);
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(c1);
        }
        table.setHeaderRows(1);
    }
    public static void addToTable(PdfPTable table, String data){

        BaseFont bf1 = null;

        {
            try {
                  bf1 = BaseFont.createFont("c:/windows/fonts/arial.ttf",
                        BaseFont.CP1250, BaseFont.CACHED);
            } catch (DocumentException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        Font polskieFonty = new Font(bf1,12);



        Font f = new Font(bf1,12);

        table.addCell(new Phrase(data,polskieFonty ));
    }
    public static Paragraph getParagraph(){
        Paragraph paragraph = new Paragraph();
        paragraph.setFont(NORMAL_FONT);
        addEmptyLine(paragraph, 1);
        return paragraph;
    }
}









