package main.globalhotkeys;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import main.controller.Controller;
import org.jnativehook.keyboard.NativeKeyEvent;
import org.jnativehook.keyboard.NativeKeyListener;

import java.io.IOException;
import java.sql.SQLException;


public class GlobalHotKeys implements NativeKeyListener {

    Controller c = new Controller();
    private boolean a = false, w = false;


    @Override
    public void nativeKeyPressed(NativeKeyEvent e) {
        Platform.runLater(() -> {
            if (e.getKeyCode() == NativeKeyEvent.VC_ALT_L) {
                a = true;
                if (w) {
                    System.out.println("alt+w");
                } else {
                    System.out.println("Only alt");
                }
            } else if (e.getKeyCode() == NativeKeyEvent.VC_W) {
                w = true;
                if (a) {
                    try {
                        c.inputButton.fire();
                        c.setOnAction(new ActionEvent());
                    } catch (IOException | SQLException ex) {
                        ex.printStackTrace();
                    }
                    System.out.println("Alt+W");

                } else {
                    System.out.println("Only W");
                }
            }
        });
    }


    @Override
    public void nativeKeyReleased(NativeKeyEvent e) {
        Platform.runLater(() -> {
            if (e.getKeyCode() == NativeKeyEvent.VC_ALT_L) {
                a = false;
            } else if (e.getKeyCode() == NativeKeyEvent.VC_W) {
                w = false;
            }
        });
    }


    @Override
    public void nativeKeyTyped(NativeKeyEvent nativeKeyEvent) {

    }


}
