package main.sqlite;

import main.model.WikiModel;


import java.sql.*;
import java.time.LocalDate;

public class SQLiteDic {

    private static Connection con;
    private static boolean hasData = true;

    public ResultSet getResultSet() {
        return resultSet;
    }

    public void setResultSet(ResultSet resultSet) {
        this.resultSet = resultSet;
    }

    ResultSet resultSet = null;

    public SQLiteDic() {

        try {
            getConnection();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }


    public ResultSet displayWords() throws SQLException, ClassNotFoundException {

        ResultSet resultSet = null;
        if (con == null) {

            getConnection();

        }

        Statement state = con.createStatement();
        try {
            resultSet = state.executeQuery("SELECT word, translation FROM dictionary");
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return resultSet;
    }

    public void getWordsBetweenDates(LocalDate beginning, LocalDate end) throws SQLException, ClassNotFoundException {

        LocalDate now = LocalDate.now();
        LocalDate start = LocalDate.now().minusDays(15);

        ResultSet resultSet1 = null;
        if (con == null) {

            getConnection();

        }

        Statement state = con.createStatement();
        try {
            resultSet = state.executeQuery("select * from dictionary where `d1` >= '" + start + "' and `d1` <= '" + end + "'");

        } catch (SQLException e) {
            e.printStackTrace();


        }


    }







    private void getConnection() throws ClassNotFoundException {


        Class.forName("org.sqlite.JDBC");

        try {
            con = DriverManager.getConnection("jdbc:sqlite:dictionary.db");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        initialise();

    }

    private void initialise() {


        System.out.println("z initialise");

        hasData = true;
        Statement stat = null;
        try {
            stat = con.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            ResultSet res = stat.executeQuery("SELECT name FROM sqlite_master WHERE type='table' and name='dictionary'");
            if (!res.next()) {

                System.out.println("initialise db");

                System.out.println("Building dictionary data base");
                Statement state1 = con.createStatement();
                state1.execute("CREATE TABLE dictionary(d1 text," + "word varchar(244)," + "translation varchar(600));");




            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            try {
                stat.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }


    public void addWord(String word, String blob) throws SQLException {


        PreparedStatement prep = null;
        if (con == null) {
            try {
                getConnection();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        try {

            long l = System.currentTimeMillis();
            Date d = new Date(l);

            prep = con.prepareStatement("INSERT INTO dictionary " +
                    "values(date('now', 'localtime'),?,?);");



            prep.setString(1, word);
            prep.setString(2, blob);
            prep.execute();

        } catch (SQLException e) {
            e.printStackTrace();

        } finally {

            if (con != null) {
                try {

                    con.close();
                    System.out.println("Word added to db");

                } catch (SQLException e) {
                    e.printStackTrace();
                }

            }

        }
    }
    }
