package main.itemsonpane;

import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;

public abstract class TranslationPane  {


    public abstract Font setFont(String fontName, FontWeight fontWeight, FontPosture fontPosture, int sizeOfFont);
    public abstract String body(String text);




}
