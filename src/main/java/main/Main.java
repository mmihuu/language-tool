package main;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
/*import main.flashcards.CreateSetOfFlashcards;*/
import main.globalhotkeys.GlobalHotKeys;
import main.sqlite.SQLiteDic;
import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;

import javax.imageio.ImageIO;
import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

public class Main extends Application {

    //icon for the background

    static String iconImageLoc;

    private Timer notificationTimer = new Timer();


    private DateFormat timeFormat = SimpleDateFormat.getTimeInstance();

    Stage stageToTray;
    static Date date;

    public static void main(String[] args) throws SQLException, ClassNotFoundException {

        String pathToAFolderWithProject = System.getProperty("user.dir");


        iconImageLoc ="file:///"+ pathToAFolderWithProject + "/src/main/resources/iconLanguage.png";


        LocalDate localDate = LocalDate.now();

        date = Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());





        System.out.println(date + " ===================");
        System.out.println(localDate);




        try {
            GlobalScreen.registerNativeHook();
        } catch (NativeHookException ex) {
            System.err.println("There was a problem registering the native hook.");
            System.err.println(ex.getMessage());

            System.exit(1);
        }

        LogManager.getLogManager().reset();
        Logger logger = Logger.getLogger(GlobalScreen.class.getPackage().getName());
        logger.setLevel(Level.OFF);

        GlobalScreen.addNativeKeyListener(new GlobalHotKeys());

       /* CreateSetOfFlashcards createSetOfFlashcards = new CreateSetOfFlashcards();
        createSetOfFlashcards.resultsDates();*/


        SQLiteDic sqLiteDic = new SQLiteDic();
        ResultSet resultSet = sqLiteDic.displayWords();
        while(resultSet.next()){
            System.out.println("tutaj while");
            System.out.println(resultSet.getString(1));
        }


        ResultSet rs;






        launch(args);


    }




    @Override
    public void start(Stage stage) throws Exception {

        stageToTray = stage;

        Platform.setImplicitExit(false);


        javax.swing.SwingUtilities.invokeLater(this::addAppToTray);

        FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/fxml/mainpane.fxml"));




        VBox bp = null;

        try {
            bp = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }


        Scene scene = new Scene(bp);



        stage.setScene(scene);
        stage.getIcons().
                add(new Image(iconImageLoc));


        stage.hide();





    }
    private void addAppToTray() {
        try {

            java.awt.Toolkit.getDefaultToolkit();


            //for the systems that dont support tray
            if (!java.awt.SystemTray.isSupported()) {
                System.out.println("No system tray support, application exiting.");
                Platform.exit();
            }


            java.awt.SystemTray tray = java.awt.SystemTray.getSystemTray();
            URL imageLoc = new URL(
                    iconImageLoc
            );
            java.awt.Image image = ImageIO.read(imageLoc);
            java.awt.TrayIcon trayIcon = new java.awt.TrayIcon(image);
            trayIcon.setImageAutoSize(true);


            trayIcon.addActionListener(event -> Platform.runLater(this::showStage));

            java.awt.MenuItem exitItem = new java.awt.MenuItem("Exit");
            exitItem.addActionListener(event -> {
                notificationTimer.cancel();
                Platform.exit();
                tray.remove(trayIcon);
            });

            // setup the popup menu for the application.
            final java.awt.PopupMenu popup = new java.awt.PopupMenu();
/*            popup.add(openItem);*/
            popup.addSeparator();
            popup.add(exitItem);
            trayIcon.setPopupMenu(popup);

            // create a timer which periodically displays a notification message.
            notificationTimer.schedule(
                    new TimerTask() {
                        @Override
                        public void run() {
                            javax.swing.SwingUtilities.invokeLater(() ->
                                    trayIcon.displayMessage(
                                            "hello",
                                            "Its time to take a test " + timeFormat.format(new Date()),
                                            java.awt.TrayIcon.MessageType.INFO
                                    )
                            );
                        }
                    }, date,
                    60_00000
            );

            // add the application tray icon to the system tray.
            tray.add(trayIcon);
        } catch (java.awt.AWTException | IOException e) {
            System.out.println("Unable to init system tray");
            e.printStackTrace();
        }
    }

    /**
     * Shows the application stage and ensures that it is brought ot the front of all stages.
     */
    private void showStage() {
        if ( stageToTray != null) {
            stageToTray.show();
            stageToTray.toFront();
        }
    }




}
