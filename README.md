# Language Tool App

As a big fan of Spanish I had to find a way to organise all these words that
I dont understand while reading websites etc. Language Tool has few modules that
help me with this objective.

First of all. Its checks for translation of a given word. All process is 
done by shortcut in the background. Pop out window gives me quick look on 
the translation.
 
 ![alt text](src/main/resources/readme/Translation.gif)
 
Secondly, it allows me to keep words and translation 
 in the database- Sqlite. Later it can be used to create flashcards
  out of the given time span. There is a friendly reminder set, so
  i don`t forget to take a test every Sunday.
 
 ![alt text](src/main/resources/readme/flashcards.gif)
 
 
 Vocabulary test functionality measure how many words you remember.
 
 ![alt text](src/main/resources/readme/test.gif)
 
 

## Installation

Project written in Java 11. You have to add JavaFX since its not 
included in this release. I had jars on my pc. Maven should 
work also just fine. Below you will find a link for a clone request

```bash
git clone https://mmihuu@bitbucket.org/mmihuu/language-tool.git
```

## Usage

This app will run in the background. Its main purpose is to organise
mess with flashcards and words that i try to memorize and help me
with my Spanish. There is still some work needed.   









## To-Do List
-   test functionality
-   flashcard functionality



 